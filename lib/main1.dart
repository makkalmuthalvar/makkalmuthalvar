import 'package:ainrc/webview.dart';
import 'package:flutter/material.dart';

class MyApp1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'N🍋R',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.green[700],
        accentColor: Colors.green[600],
      ),
      home: MyHomePage(title: 'N 🍋 R'),
    );
  }
}
